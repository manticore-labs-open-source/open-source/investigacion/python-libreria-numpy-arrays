# Python librería NumPy Arrays

## Índice
1. [ Numpy librería introducción](#id1)
2. [Numpy Array](#id2)
3. [Creando Arrays NumPy]( #id3)


<a name="id1"></a>
## Introducción 
la librería NumPy sus siglas significan (NumPy, Numerical Python) es un paquete con un alto nivel matemático y funciones y herramientas numéricas están designadas  para trabajar el numply array objeto. Un numpy array tiene un alto rendimiento multidimensional estructura de datos.
Un programa Python usando la librería numpy podría requerir instalar el paquete. Adicionalmente, un programa que usa el paquete debería incluir la siguiente sentencia 
~~~
import numpy
~~~

<a name="id2"></a>
## Numpy Array  
Python trata principalmente con listas y no tiene una estructura de array nativa, por lo que para entender el array  numpy es importante tener una comprensión básica de los arrays.
Un array de una dimensión puede estar directamente comparado a una lista Python porque ambos almacenan una lista de números, igual que en otros arrays estos son accesados con **[]**.
De dos dimensiones tiene una estructura con filas y columnas.
De tres dimensiones puede estar pensado como una cola de arrays de dos dimensione.
Los Numpy arrays son de un tamaño fijo, el índice empieza en 0, y contiene elemento de un mismo tipo. son **ndarray** objetos y son creados usando la función **np.array()** por ejemplo:
~~~
import numpy as np
b = np.array([1,2,3,4])
~~~
 Este ejemplo crea un array de una dimensión con 4 constantes. Los arrays Numpy se refieren a las dimensiones como **axes** y el numero de axes en el **rank**. Entonces el arrrays declaro como **b** es de rango 1.
 
 Hay varias funciones numpy para describir a los arrays y sus elementos, como se demuestra a continuación:
 ~~~
import numpy as np

# Create an array of rank 2
arreglo = np.array([[1,2,3,4], [5,6,7,8]])

print(arreglo)
# [[1 2 3 4]
# [5 6 7 8]]

print(arreglo[1, 2]) # accediendo a un elemento
# 7

print(arreglo.ndim) # la dimensión pero conocido como rango
# 2

print(arreglo.shape) # n filas , m columnas
# (2, 4)

print(arreglo.size) # numero de elementos 
# 8

print(type(arreglo)) # tipo de elemento 
# <type 'numpy.ndarray'>
~~~
El uso de corchetes se da para la creación y para acceder a los elementos.

<a name="id3"></a>

## Creando Arrays NumPy
NumPy incluye varias funciones para crear arrays. usted puede inicializar arrays de unos o ceros o con valores randomicos por ejemplo:
~~~
import numpy as np

a = np.ones((3,2))  # array de unos 
print(a)

b = np.zeros((3,4)) # array de zeros 
print(b)

c = np.random.random(3) # array de valores randomicos 
print(c)

d = np.full((2,2),12) # arrays con valores constantes
print(d)
~~~
 
También esta disponible la función np.empty() para crear un array no inicializado. las funciones np.eye() y np.identity esto utilizado para la matriz identidad. es opcional definir el tipo de datos de la siguiente manera.
~~~
d = np.fully((2,2),12,dtype = np.float32)
~~~

la función **copy(9)** crea la copia de un arrays,
la función **loadtxt()** es utilizada para cargar datos desde un archivo,
la función **save()** permite guardar en un archivo dicho arreglo.





## Redes Sociales
<a href="https://twitter.com/crisjc8" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @crisjc8 </a><br>
<a href="https://linkedin.com/in/cristhian-jumbo-748934180/" target="_blank"><img alt="Sígueme en LinkedIn" height="35" width="35" src="https://4.bp.blogspot.com/-0KtSvK3BydE/XCrIzgI3RqI/AAAAAAAAH_w/n_rr5DS92uk9EWEegcxeqAcSkV36OWEOgCLcBGAs/s1600/linkedin.png" title="Sígueme en LinkedIn"/> Cristhian Jumbo</a><br>
<a href="https://www.instagram.com/crisjc6/" target="_blank"><img alt="Sígueme en Instagram" height="35" width="35" src="https://4.bp.blogspot.com/-Ilxti1UuUuI/XCrIy6hBAcI/AAAAAAAAH_k/QV5KbuB9p3QB064J08W2v-YRiuslTZnLgCLcBGAs/s1600/instagram.png" title="Sígueme en Instagram"/> crisjc6</a><br>
